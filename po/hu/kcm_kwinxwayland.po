# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# SPDX-FileCopyrightText: 2024 Kristof Kiszel <ulysses@fsf.hu>
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2024-01-11 12:44+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ui/main.qml:47
#, kde-format
msgid ""
"Legacy X11 apps require the ability to read keystrokes typed in other apps "
"for features that are activated using global keyboard shortcuts. This is "
"disabled by default for security reasons. If you need to use such apps, you "
"can choose your preferred balance of security and functionality here."
msgstr ""
"Az örökölt X11 alkalmazásoknak szüksége van a más alkalmazásokban írt "
"billentyűleütések olvasására a globális billentyűparancsokkal aktivált "
"szolgáltatásokhoz. Ez biztonsági okokból alapértelmezetten ki van kapcsolva. "
"Ha ilyen alkalmazásokat szeretne használni, itt kiválaszthatja a biztonság "
"és a funkcionalitás kívánt egyensúlyát."

#: ui/main.qml:64
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr ""
"Más alkalmazásokban beírt billentyűleütések olvasásának engedélyezése "
"örökölt X11 alkalmazásoknak:"

#: ui/main.qml:65
#, kde-format
msgid "Never"
msgstr "Soha"

#: ui/main.qml:68
#, kde-format
msgid "Only non-character keys"
msgstr "Csak nem karakter billentyűk"

#: ui/main.qml:71
#, kde-format
msgid ""
"As above, plus any key typed while the Control, Alt, or Meta keys are pressed"
msgstr ""
"Mint fent, plusz bármely billentyű, amelyet a Control, Alt vagy Meta "
"billentyűk lenyomva tartása közben gépeltek be"

#: ui/main.qml:75
#, kde-format
msgid "Always"
msgstr "Mindig"

#: ui/main.qml:82
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Ne feledje, hogy ennek a beállításnak a használata lecsökkenti a rendszer "
"biztonságát az X11 munkamenet szintjére azzal, hogy lehetővé teszi "
"rosszindulatú szoftvereknek jelszavak ellopását vagy a begépelt szöveg utáni "
"kémkedést. Győzödjön meg róla, hogy megértette és elfogadja ezt a kockázatot."
